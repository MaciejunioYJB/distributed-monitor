#include "./program/program.h"
#include <thread>
#include <iostream>

int main(int argc, char **argv) {
    if (argc != 2) {
        std::cout << "Invalid number of parameters" << std::endl;
        return -1;
    }
    int workerId = std::stoi(argv[1]); 
    callProgram(workerId);
    return 1;
}
