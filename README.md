# Distributed Monitor 

Author: Maciej Łaszkiewicz

This project simulates work of distributed monitor, based on [suzuki-kasami algorithm](https://en.wikipedia.org/wiki/Suzuki%E2%80%93Kasami_algorithm). To communication between programs used [ZMQ](https://zeromq.org/).

## Compiling:

``
g++ -o main main.cpp program/program.cpp program/monitor.cpp -lzmq -lpthread
``

## Run:

run on three consoles in the same time with three different arguments (1-3):

``
./main 1
``
``
./main 2
``
``
./main 3
``

Every id is assigned to different program with different amount of tasks.