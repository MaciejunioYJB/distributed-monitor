#ifndef MONITOR_H
#define MONITOR_H
#include <vector>
#include <queue>
#include <string>
#include "zmq.h"

class Monitor {
    private:
        //defaults
        const std::string PROTOCOL = "tcp";
        const std::string ADDRESS = "127.0.0.1";
        //variables
        std::vector<std::pair<int,int>> RN; //request number array
        std::vector<int> LRN; //last request number array
        std::queue<int> requestQueue;
        int port;
        bool token = false;
        void *context;
        void *socket;
        std::string host;

        void sendMessage(std::string message, int receiverPort, std::string options);
        void listener();
        bool findInQueue(std::queue<int> queue,int value);
        std::string queueToString(std::queue<int> queue);
        std::string vectorToString(std::vector<int> vector);
        std::vector<std::string> split(const std::string &s, char separator, std::vector<std::string> &elements);

    public:
        Monitor(int id, std::vector<int> ports);
        ~Monitor();
        void entry();
        void exit();
};

#endif