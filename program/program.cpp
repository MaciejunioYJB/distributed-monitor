#include "program.h"
#include "monitor.h"
#include <iostream>
#include <thread>
#include <fstream>

void callProgram(int id) {
    std::cout << "Starting program: " << id << std::endl;
    std::vector<int> portPoll = {8088, 8089, 8090};
    switch (id) {
        case 1: {
            startProgram(id, 4, portPoll);
            break;
        }
        case 2: {
            startProgram(id, 3, portPoll);
            break;
        }
        case 3: {
            startProgram(id, 2, portPoll);
            break;
        }
        default: {
            throw "Wrong number of program. Please provide between 1 and 3";
            break;
        }
    }
}

void startProgram(int id, int counter, std::vector<int> ports) {
    Monitor monitor = {id, ports};
    std::cout << "Ready to start\n" << std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    for (int i=0; i<counter; i++) {
        std::cout << "Program " << id << ": Task " << i+1 << std::endl;
        monitor.entry();
        work(id);
        monitor.exit();
        nap();
    }
    std::cout << "Program " << id << " has endend " << counter << " tasks" << std::endl;
}

void work(int id) {
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    std::ofstream file;
    file.open ("file.txt", std::ios::app);
    file << "Process " << id << "\n";
    file.close();
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
}

void nap() {
    //sleep after work for a while
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
}