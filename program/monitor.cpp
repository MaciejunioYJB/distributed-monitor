#include "monitor.h"
#include <string>
#include <thread>
#include <cstring>
#include <iostream>
#include <sstream>
#include <chrono>

Monitor::Monitor(int id, std::vector<int> ports) {
    if (id == 1) {
        token = true;
    }
    //ZMQ setup
    context = zmq_ctx_new();
    socket = zmq_socket(context, ZMQ_REP);

    //network setup
    port = ports[id-1];
    host = PROTOCOL + "://" + ADDRESS + ":" + std::to_string(port);
    std::cout << "Running on addres: " << ADDRESS + ":" + std::to_string(port) << std::endl;
    zmq_bind(socket, host.c_str());

    //arrays initialization
    for (const int &thisPort : ports) {
        RN.push_back(std::make_pair(thisPort,0));
        LRN.push_back(0);
    }
    //Listener initialization
    std::thread handler(&Monitor::listener, this);
    handler.detach();
};

void Monitor::entry() {
    std::cout << "Trying to entry critical section" << std::endl;
    if (!token) {
        int requestNumber = 0;
        //iterate and bump request number for current program
        for (auto reuqestIt = begin (RN); reuqestIt != end (RN); ++reuqestIt) {
            if (reuqestIt->first == port) { 
                reuqestIt->second++;
                requestNumber = reuqestIt->second;
            }
        }
        //iterate and send request message to the others
        for (auto reuqestIt = begin (RN); reuqestIt != end (RN); ++reuqestIt) {
            if (reuqestIt->first != port) {
                sendMessage("REQUEST;" + std::to_string(port), reuqestIt->first, std::to_string(requestNumber));
            }
        }
        while (!token) {
            //waiting for token
            std::this_thread::sleep_for(std::chrono::milliseconds(200));
        }
    }
    auto now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    std::cout << "Entered critical section" << std::endl 
              << "\033[1;33m========== CS ========== " << std::ctime(&now) << "\033[0m";
};

void Monitor::exit() {
    std::cout << "Leaving critical section" << std::endl;
    //adding incoming requests to queue
    for (unsigned int n = 0; n < RN.size(); n++) {
        if (RN[n].second == LRN[n]+1) {
            if (!findInQueue(requestQueue, RN[n].first)) {
                requestQueue.push(RN[n].first);
            }
            LRN[n]++;
        }
    }
    //giveaway critical section if queue is not empty
    if (requestQueue.size() > 0) {
        token = false;
        int port_ = requestQueue.front();
        requestQueue.pop();
        std::string options = queueToString(requestQueue) + ";" + vectorToString(LRN);
        sendMessage("RELEASE;" + std::to_string(port), port_, options);
    }
    std::cout << "Leaved critical section" << std::endl;
};

Monitor::~Monitor() {
    zmq_close(socket);
    zmq_ctx_destroy(context);
};


/* ~~ Helpers - nonpublic methods: ~~ */

void Monitor::listener() {
    //operating on 128 bytes buffer
    char *buffer = new char[128];
    while (true) {
        memset(buffer, 0, 128);
        //receiving a message
        if (zmq_recv(socket, buffer, 128, 0) > -1) {
            zmq_send(socket, "", 0, 0);
            std::string receivedMsg = std::string(reinterpret_cast<char const *>(buffer), std::char_traits<char>::length(buffer));
            std::cout << "\033[1;31mReceived: " << receivedMsg << "\033[0m" << std::endl;
            std::vector<std::string> receivedBits;
            split(receivedMsg, ';', receivedBits);
            
            //RELEASE - 'token is mine'
            if (receivedMsg.find("RELEASE") != std::string::npos) {
                token = true;
                std::vector<std::string> receivedLNR;
                split(receivedBits[3], ',', receivedLNR);
                for (int i = 0; i < receivedLNR.size(); i++) {
                    LRN[i] = std::stoi(receivedLNR[i]);
                }
                std::vector<std::string> receivedQueue;
                split(receivedBits[2], ',', receivedQueue);
                requestQueue = {};
                if(!receivedQueue.empty()) {
                    if (receivedQueue.size() == 1) {
                        requestQueue.push(std::stoi(receivedQueue[0]));
                    } else {
                        for (int i = receivedQueue.size(); i < 1; i--) {
                            requestQueue.push(std::stoi(receivedQueue[i-1]));
                        }
                    }
                }
            }

            //REQUEST - 'somebody wants a token, notice that'
            if (receivedMsg.find("REQUEST") != std::string::npos) {
                int port_ = std::stoi(receivedBits[1]);
                int requestNumber = std::stoi(receivedBits[2]);
                for (auto requestIt = begin (RN); requestIt != end (RN); ++requestIt) {
                    if (requestIt->first == port_) {
                        if (requestIt->second < requestNumber) {
                            requestIt->second = requestNumber;
                        }
                    }
                }
            }
        }
    }
}

void Monitor::sendMessage(std::string message, int receiverPort, std::string options) {
    void *socket_ = zmq_socket(context, ZMQ_REQ);
    std::string receiver = PROTOCOL + "://" + ADDRESS + ":" + std::to_string(receiverPort);
    if (zmq_connect(socket_, receiver.c_str()) == 0) {
        //creating message in a given pattern: TYPE_OF_MESSAGE;RECEIVER_PORT;OPTIONS
        //where options is requestNumber or request queue and LNR array
        std::string requestMsg = message + ";" + options;
        std::cout << "\033[1;32mSending: " << requestMsg << " to " << receiver << "\033[0m" << std::endl;
        zmq_send(socket_, requestMsg.c_str(), requestMsg.size(), 0);
    } else {
        std::cout << zmq_strerror(zmq_errno()) << std::endl;
    }
    zmq_close(socket_);
}

bool Monitor::findInQueue(std::queue<int> queue, int value) {
    int item;
    while (queue.size() > 0){
        item = queue.front();
        if (item == value) {
            return true;
        }
        queue.pop();
    }
    return false;
}

std::string Monitor::queueToString(std::queue<int> queue) {
    std::string stringQueue = "";
    if (queue.empty()) {
        return stringQueue;
    }
    while (queue.size() > 0){
        stringQueue = stringQueue + std::to_string(queue.front()) + ",";
        queue.pop();
    }
    stringQueue.pop_back();
    return stringQueue;
}

std::string Monitor::vectorToString(std::vector<int> vector) {
    std::string stringVector = "";
    for (auto &element : vector) {
        stringVector = stringVector + std::to_string(element) + ",";
    }
    stringVector.pop_back();
    return stringVector;
}

std::vector<std::string> Monitor::split(const std::string &s, char separator, std::vector<std::string> &elements) {
    std::stringstream stream(s);
    std::string item;
    if (s.find(separator) != std::string::npos){
        while (std::getline(stream, item, separator)) {
            elements.push_back(item);
        }
    }
    else if (!s.empty()){
        elements.push_back(s);
    }
    return elements;
}
