#ifndef PROGRAM_H
#define PROGRAM_H
#include <vector>

void callProgram(int id);
void startProgram(int id, int counter, std::vector<int> ports);
void work(int id);
void nap();

#endif